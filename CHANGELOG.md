# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 1.3.0
- Update dependencies for Magento 2.3

## 1.2.0
- Update dependency for Magento 2.2.3
- Remove php dependency, it is an implicit requirement of magento
