<?php

namespace Bitbull\ProvinceItaliane\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Customer\Model\ResourceModel\Address\CollectionFactory;
use Magento\Directory\Model\RegionFactory;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var DirectoryHelper
     */
    private $directoryHelper;
    
    /**
     * @var WriterInterface
     */
    private $writerInterface;
    
    /**
     * @var CollectionFactory
     */
    protected $addressesFactory;
    
    /**
     * @var RegionFactory
     */
    protected $regionFactory;

    /**
     * Init
     *
     * @param DirectoryHelper $directoryHelper
     * @param WriterInterface $writerInterface
     * @param CollectionFactory $addressesFactory
     * @param RegionFactory $regionFactory
     */
    public function __construct(
        DirectoryHelper $directoryHelper,
        WriterInterface $writerInterface,
        CollectionFactory $addressesFactory,
        RegionFactory $regionFactory
    ) {
        $this->directoryHelper = $directoryHelper;
        $this->writerInterface = $writerInterface;
        $this->addressesFactory = $addressesFactory;
        $this->regionFactory = $regionFactory;
    }
    
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // Upgrade from 1.0.0 to 1.0.1
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $this->upgradeVersionOneZeroOne();
        }
        
        // Upgrade from 1.0.1 to 1.0.2
        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $this->upgradeVersionOneZeroTwo();
        }
    }
    
    /**
     * Upgrade data to version 1.0.1
     * Set region as mandatory for italian addresses
     */
    protected function upgradeVersionOneZeroOne()
    {
        $countries = $this->directoryHelper->getCountriesWithStatesRequired();
        if(!in_array('IT', $countries)) {
            $countries []= 'IT';
            
            $this->writerInterface->save(
                DirectoryHelper::XML_PATH_STATES_REQUIRED,
                implode(',', $countries)
            );
        }
    }
    
    /**
     * Upgrade data to version 1.0.2
     * Set correct region id where possible, for italian addresses
     */
    protected function upgradeVersionOneZeroTwo()
    {
        // Load italian addresses where region_id is not set
        $addressCollection = $this->addressesFactory->create();
        $addressCollection->addFilter('country_id', 'IT');
        $addressCollection->addFilter('region_id', 0);
        
        // Try to assign the region_id
        $regionsByCode = [];
        $regionsByName = [];
        
        foreach($addressCollection as $address) {
            $regionId = 0;
            $regionField = $address->getRegion();
            $region = $this->regionFactory->create();
            
            // Try to look between the regions that have already been loaded
            if(isset($regionsByCode[$regionField])) {
                $regionId = $regionsByCode[$regionField];
            } elseif(isset($regionsByName[$regionField])) {
                $regionId = $regionsByName[$regionField];
            }
            
            // Try from database
            if(!$regionId) {
                $region->loadByCode($regionField, 'IT');
                if(!$region->getId()) {
                    $region->loadByName($regionField, 'IT');
                }
                
                if($region->getId()) {
                    $regionId = $region->getId();
                    $regionsByCode[$region->getCode()] = $regionId;
                    $regionsByName[$region->getName()] = $regionId;
                }
            }
            
            if(!$regionId) {
                continue;
            }
            
            $address->setRegionId($regionId);
            $address->save();
        }
    }
}
