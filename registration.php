<?php

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE,
    'Bitbull_ProvinceItaliane',
    __DIR__
);
